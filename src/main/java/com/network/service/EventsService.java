package com.network.service;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

import com.network.entity.Event;
import com.network.storage.EventDao;
import com.sun.jersey.api.client.ClientResponse.Status;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.QNameMap;
import com.thoughtworks.xstream.io.xml.StaxDriver;

//maps this resource to the URL events
@Path("/events")
public class EventsService {

	// Allows to insert contextual objects into the class
	@Context
	UriInfo uriInfo;
	@Context
	Request request;

	public EventsService() {
		super();
		QNameMap qnameMap = new QNameMap();
		QName qname = new QName(
				"http://localhost:8080/RestSimpleApp/rest/events", "o");
		qnameMap.registerMapping(qname, Event.class);
		xstream = new XStream(new StaxDriver(qnameMap));
		xstream.alias("event", Event.class);
	}

	private final XStream xstream;

	// Return the list of events for applications with json or xml formats
	@GET
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public List<Event> getEvents() {
		List<Event> events = new ArrayList<Event>();
		events.addAll(EventDao.getInstance().getModel().values());
		return events;
	}

	@GET
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Path("/{eventId}")
	public String getEvent(@PathParam("eventId") String eventId) {

		Event event = EventDao.getInstance().getModel().get(eventId);

		if (event == null) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		} else {
			try {
				return xstream.toXML(event);
			} catch (Exception e) {
				throw new WebApplicationException(
						Response.Status.INTERNAL_SERVER_ERROR);
			}
		}
	}

	@POST
	public Response postEvent(String eventXml) {
		try {
			Event event = (Event) xstream.fromXML(eventXml);
			EventDao.getInstance().getModel().put(event.getId(), event);

			Response response = Response.created(
					new URI(uriInfo.getAbsolutePath() + event.getId())).build();
			return response;

		} catch (Exception ex) {
			throw new WebApplicationException(
					Response.Status.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_XML)
	public Response putTodo(JAXBElement<Event> event) {
		Event o = event.getValue();
		return putAndGetResponse(o);
	}

	private Response putAndGetResponse(Event todo) {
		Response res;
		if (EventDao.getInstance().getModel().containsKey(todo.getId())) {
			res = Response.status(Status.OK).build();
		} else {
			res = Response.status(Status.NOT_FOUND).build();
		}
		EventDao.getInstance().getModel().put(todo.getId(), todo);
		return res;
	}

	@DELETE
	@Path("/{eventId}")
	public Response deleteEvent(@PathParam("eventId") String eventId) {
		try {
			Event removedEvent = EventDao.getInstance().getModel().remove(eventId);
			if (removedEvent == null) {
				return Response.status(Status.NOT_FOUND).build();
			} else {
				return Response.ok().entity(removedEvent).build();
			}
		} catch (Exception e) {
			return Response.serverError().build();
		}
	}

}