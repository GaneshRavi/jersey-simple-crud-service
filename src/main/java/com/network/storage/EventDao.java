 package com.network.storage;

import java.util.HashMap;
import java.util.Map;

import com.network.entity.Event;

public class EventDao {
       private static Map<String, Event> contentProvider = new HashMap<>();
       private static final EventDao INSTANCE = new EventDao();

       private EventDao() {
              Event event = new Event("1", "First event");
              event.setDescription("A new object was added to the network");
              contentProvider.put("1", event);
              event = new Event("2", "Second event");
              event.setDescription("An object was deleted from the network");
              contentProvider.put("2", event);
       }
      
       public Map<String, Event> getModel(){
              return contentProvider;
       }
       
       public static EventDao getInstance() {
    	   return INSTANCE;
       }
       
}