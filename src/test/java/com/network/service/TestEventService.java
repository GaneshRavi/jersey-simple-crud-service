package com.network.service;

import java.net.URI;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.network.entity.Event;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.ClientResponse.Status;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

public class TestEventService {

	private static WebResource service;

	@BeforeClass
	public static void setUp() {
		ClientConfig config = new DefaultClientConfig();
		Client client = Client.create(config);
		service = client.resource(getBaseURI());
	}

	private static URI getBaseURI() {
		return UriBuilder.fromUri("http://localhost:8080/RestSimpleApp").build();
	}

	@Test
	public void create() {
		// create one todo
		Event event = new Event("3", "Blabla");
		ClientResponse response = service.path("rest").path("events")
				.accept(MediaType.APPLICATION_XML)
				.post(ClientResponse.class, event);
		// Return status must be CREATED == created resource
		Assert.assertEquals(response.getStatus(), (Status.CREATED).getStatusCode());
	}
	
	@Test (dependsOnMethods = {"create"})
	public void read() {
		// Get XML for application
				System.out.println("All events: " + service.path("rest").path("events")
						.accept(MediaType.APPLICATION_XML).get(String.class));

		// Get the Todo with id 3
		System.out.println("Event 3: " + service.path("rest").path("events/3")
				.accept(MediaType.APPLICATION_XML).get(String.class));
	}

	@Test (dependsOnMethods = {"read"})
	public void update() {
		// Put updated Event with id 3
		Event event = new Event("3", "Updated Blabla");
		ClientResponse response = service.path("rest").path("events")
				.accept(MediaType.APPLICATION_XML).put(ClientResponse.class, event);
		// Return status must be OK == updated resource
		Assert.assertEquals(response.getStatus(), (Status.OK).getStatusCode());
	}

	@Test (dependsOnMethods = {"update"})
	public void delete() {
		// Delete Todo with id 3
		System.out.println("Response for deleting 3: " + service.path("rest").path("events/3").delete(String.class));

		// Get the all todos, id 3 should be deleted
		System.out.println("All events: " + service.path("rest").path("events")
				.accept(MediaType.APPLICATION_XML).get(String.class));
	}

} 
